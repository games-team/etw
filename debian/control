Source: etw
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libgtk2.0-dev,
 libsdl1.2-dev
Standards-Version: 4.6.1
Homepage: http://www.ggsoft.org/etw/
Vcs-Git: https://salsa.debian.org/games-team/etw.git
Vcs-Browser: https://salsa.debian.org/games-team/etw

Package: etw
Architecture: any
Depends:
 etw-data (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: arcade-style soccer game
 Eat The Whistle is an arcade soccer game similar to famous Amiga titles such
 as Kick Off or Sensible Soccer. It features several game modes where you can
 play either as the whole team or as a single player, and you can also manage
 teams that take part in cups and leagues. There is even an arcade mode with
 powerups and bonuses, like in the game SpeedBall 2.
 .
 Eat The Whistle features 30 different field types and numerous sound effects.
 The game is viewed from the side and can be controlled with either a joystick
 or the keyboard.
 .
 Most in-game settings are configurable, such as the pitch, weather and game
 daytime, which will impact on the gameplay. There is a replay mode that lets
 you load and save best moments, a game tactics editor, and teams from the
 game Sensible World of Soccer can be directly imported.

Package: etw-data
Architecture: all
Depends:
 ${misc:Depends}
Suggests:
 etw
Description: graphics and audio data for etw
 Eat The Whistle is an arcade soccer game similar to famous Amiga titles such
 as Kick Off or Sensible Soccer. It features several game modes where you can
 play either as the whole team or as a single player, and you can also manage
 teams that take part in cups and leagues. There is even an arcade mode with
 powerups and bonuses, like in the game SpeedBall 2.
 .
 This package contains the architecture-independent data for etw. For more
 information, see the etw package.
